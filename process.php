<?php
require_once('config.php');
?>
<?php

if(isset($_POST)){
	$id 		= $_POST['id'];
	$firstname 		= $_POST['firstname'];
	$lastname 		= $_POST['lastname'];
	$email 			= $_POST['email'];
	$password 		= $_POST['password'];
	$gender 		= $_POST['gender'];
	$hobbies 		= $_POST['hobbies'];
	$status = 0;


		$sql = "INSERT INTO users (id,firstname, lastname, email, password ,gender,hobbies,status) VALUES(?,?,?,?,?,?,?,?)";
		$stmtinsert = $db->prepare($sql);
		$result = $stmtinsert->execute([$id ,$firstname, $lastname, $email, $password,$gender,$hobbies,$status]);
		if($result){
			echo 'Successfully saved.';
			header("location: index.php");
		}else{
			echo 'There were erros while saving the data.';
		}
}else{
	echo 'No data';
}