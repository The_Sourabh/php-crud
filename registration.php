<?php
require_once('config.php');
?>
<!DOCTYPE html>
<html>

<head>
	<title>User Registration | PHP</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<style>
* {
  box-sizing: border-box;
}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}

label {
  padding: 12px 12px 12px 0;
  display: inline-block;
}

input[type=submit] {
  background-color: #04AA6D;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  float: right;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

.col-25 {
  float: left;
  width: 25%;
  margin-top: 6px;
}

.col-75 {
  float: left;
  width: 75%;
  margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .col-25, .col-75, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}
</style>

</head>

<body>
	<div class="container">
		<form action="registration.php" method="post">
		<h1>Registration</h1>
			<div class="container">
						

						<p>Fill up the form with correct values.</p>
							<label for="id"><b>Student ID</b></label>
							<input class="form-control" id="id" type="text" name="id" required>

							<label for="firstname"><b>First Name</b></label>
							<input class="form-control" id="firstname" type="text" name="firstname" required>

							<label for="lastname"><b>Last Name</b></label>
							<input class="form-control" id="lastname" type="text" name="lastname" required>

							<label for="email"><b>Email Address</b></label>
							<input class="form-control" id="email" type="email" name="email" required>

							<label for="password"><b>Password</b></label>
							<input class="form-control" id="password" type="password" name="password" required>

							<label for="gender"><b>gender</b></label>
							<input class="form-control" id="gender" type="gender" name="gender" required>



							<label for="hobbies"><b>Hobbies</b></label>
							<input class="form-control" id="hobbies" type="hobbies" name="hobbies" required>
							<hr class="mb-3">
							<input class="btn btn-primary" type="submit" id="register" name="create" value="Submit">
							<a class = "btn btn-primary" href="index.php">Home Page</a>
							
						</div>
					</div>
				</div>
		</form>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script type="text/javascript">
		$(function() {
			$('#register').click(function(e) {



				var id = $('#id').val();
				var firstname = $('#firstname').val();
				var lastname = $('#lastname').val();
				var email = $('#email').val();
				var password = $('#password').val();
				var gender = $('#gender').val();
				var hobbies = $('#hobbies').val();


				// $.post( '',{ registerName: $('#registerName') } )

				e.preventDefault();

				$.ajax({
					type: 'POST',
					url: 'process.php',
					data: {
						id: id,
						firstname: firstname,
						lastname: lastname,
						email: email,
						password: password,
						gender: gender,
						hobbies: hobbies
					},
					success: function(data) {
						Swal.fire({
							'title': 'Successful',
							'text': data,
							'type': 'success'
						})

					},
					error: function(data) {
						Swal.fire({
							'title': 'Errors',
							'text': 'There were errors while saving the data.',
							'type': 'error'
						})
					}
				});







			});


		});
	</script>
</body>

</html>



