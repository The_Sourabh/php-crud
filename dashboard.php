
<?php

include 'config.php';
$sql = "SELECT * FROM users ORDER BY status DESC ";
$result = $con->query($sql);
$con->close();

if (isset($_POST['submit'])) {

	$sql = "UPDATE users SET status='1' WHERE id=2";
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Dashboard</title>

	<style>
		table {
			margin: 0 auto;
			font-size: large;
			border: 1px solid black;
		}

		h1 {
			text-align: center;
			color: #006600;
			font-size: xx-large;
			font-family: 'Gill Sans', 'Gill Sans MT',
			' Calibri', 'Trebuchet MS', 'sans-serif';
		}

		td {
			background-color: #E4F5D4;
			border: 1px solid black;
		}

		th,
		td {
			font-weight: bold;
			border: 1px solid black;
			padding: 10px;
			text-align: center;
		}

		td {
			font-weight: lighter;
		}
	</style>
</head>

<body>

	<section>
		<h1>Dashboard</h1>

		<table>
			<tr>
                <th>Status</th>
				<th>Student ID</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th>
                <th>Gender</th>
                <th>Action</th>
			</tr>

			<?php 
				while($rows=$result->fetch_assoc())
				{



			?>
			<tr>

                <td><?php  if($rows['status'] == 0) { echo"Pending"; } if($rows['status'] == 1) { echo"ACCEPT"; } if($rows['status'] == -1) { echo"REJECT"; } ?></td>
				<td><?php echo $rows['id'];?></td>
				<td><?php echo $rows['firstname'];?></td>
				<td><?php echo $rows['lastname'];?></td>
				<td><?php echo $rows['email'];?></td>
                <td><?php echo $rows['gender'];?></td>
                <td>
				
				<a class = "btn btn-primary" href="update.php?id=<?php echo $rows["id"]; ?>">ACCEPT</a>

				<a class = "btn btn-primary" href="delete.php?id=<?php echo $rows["id"]; ?>">REJECT</a>
				</td>
                
			</tr>
			<?php
				}
			?>
		</table>
	</section>

</body>

</html>
